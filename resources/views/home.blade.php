@extends('base')
@section('content')
    <div class="col-3">
        @foreach($categories as $category)
            <a href="{{action([\App\Http\Controllers\CategoryController::class, 'show'], ['category' => $category])}}">
                <h5>{{$category->name}}</h5>
            </a>
        @endforeach
    </div>
@endsection
