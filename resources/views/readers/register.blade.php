@extends('base')
@section('content')
    <form action="{{action([\App\Http\Controllers\ReadersController::class, 'store'])}}" method="post">
        @csrf
        <div class="mb-3">
            <label for="name" class="form-label">Name</label>
            <input type="text" class="form-control" id="name" name="name">
        </div>
        <div class="mb-3">
            <label for="address" class="form-label">Address</label>
            <input type="text" class="form-control" id="address" name="address">
        </div>
        <div class="mb-3">
            <label class="form-label" for="passport">Passport Number</label>
            <input type="text" class="form-control" id="passport" name="passport_number">
        </div>
        <div class="mb-3">
            <label class="form-label" for="password">Password</label>
            <input type="password" class="form-control" id="password" name="password">
        </div>
        <div class="mb-3">
            <label class="form-label" for="password_conf">Password Confirmation</label>
            <input type="password" class="form-control" id="password_conf" name="password_confirmation">
        </div>
        <button type="submit" class="btn btn-primary">Register</button>
    </form>
@endsection
