@extends('base')
@section('content')
    <h2>Hello, {{$reader->name}}</h2>
    <hr>
    <h3 class="text-center">Book request history</h3><br><br>
    <table>
        <thead class="bg-dark-subtle">
        <tr>
            <th><h5>Book name</h5></th>
            <th><h5>Status</h5></th>
            <th><h5>Return Date</h5></th>
        </tr>
        </thead>
        @foreach($reader->requests as $request)
            <tr>
                <td>
                    <h5>{{$request->book->title}}</h5>
                </td>
                <td>
                    <h5>{{$request->book->status}}</h5>
                </td>
                <td>
                    <h5>{{$request->return_date}}</h5>
                </td>
            </tr>
        @endforeach
    </table>

@endsection
