@extends('base')
@section('content')
    <h1>{{$category->name}}</h1>
    <div class="row justify-content-around">
        @foreach($category->books as $book)
            <div class="col-3 border border-1 m-3 p-2">
                <img src="{{asset('storage/' . $book->image)}}" alt="Book Description">
                <h3>{{$book->title}}</h3>
                <h6>{{$book->author}}</h6>

                <a href="{{action([\App\Http\Controllers\RequestController::class, 'makeRequest'], compact('book'))}}" class="btn btn-outline-info">Получить</a>
            </div>
        @endforeach

    </div>
@endsection
