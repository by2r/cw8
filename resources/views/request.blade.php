@extends('base')
@section('content')
    <div class="col-3">
        <form action="{{route('request_store')}}" method="post">
            @csrf
            @if(session()->exists('library_card_number'))
                <div class="form-group mb-3">
                    <label for="library_card_number">Library Card Number</label>
                    <input type="text" class="form-control" id="library_card_number" name="library_card_number" value="{{session()->get('library_card_number')}}" readonly>
                </div>
            @else
                <div class="form-group mb-3">
                    <label for="library_card_number">Library Card Number</label>
                    <input type="text" class="form-control" id="library_card_number" name="library_card_number">
                </div>
            @endif
            <div class="form-group mb-3">
                <label for="book">Book</label>
                <input type="text" class="form-control" id="book" name="book_id" value="{{$book}}" readonly>
            </div>
            <div class="form-group mb-3">
                <label for="return_date">Return Date</label>
                <input type="date" class="form-control" id="return_date" name="return_date">
            </div>
            <button type="submit" class="btn btn-primary">Подать заявку</button>
        </form>
    </div>
@endsection
