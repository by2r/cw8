@extends('base')

@section('content')
    <div class="row">
        <div class="col-6 offset-3 align-self-center">
            <h1 class="text-center">Log in</h1>
            <hr>
            <form action="{{route('store')}}" method="post">
                @csrf
                <div class="form-group mb-3">
                    <label for="library_card_number">Library Card Number</label>
                    <input type="text" class="form-control" id="library_card_number" name="library_card_number">
                </div>
                <div class="form-group mb-3">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button> OR  <a href="{{route('register')}}">register</a>
            </form>
        </div>
    </div>
@endsection
