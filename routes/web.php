<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::get('/', [\App\Http\Controllers\CategoryController::class, 'index'])->name('home');
//Route::get('/categories/{category_id}/', [\App\Http\Controllers\CategoryController::class, 'show'])->name('show');
Route::resource('categories', \App\Http\Controllers\CategoryController::class)->only('index', 'show');
Route::get('/register', [\App\Http\Controllers\ReadersController::class, 'register'])->name('register');
Route::post('/register', [\App\Http\Controllers\ReadersController::class, 'store']);
Route::get('/login', [\App\Http\Controllers\SessionsController::class, 'create'])->name('login');
Route::post('/login', [\App\Http\Controllers\SessionsController::class, 'store'])->name('store');
Route::delete('/logout', [\App\Http\Controllers\SessionsController::class, 'destroy'])->name('logout');
Route::get('/book_request', [\App\Http\Controllers\RequestController::class, 'makeRequest']);
Route::post('/book_request', [\App\Http\Controllers\RequestController::class, 'store'])->name('request_store');
Route::get('/my_books', [\App\Http\Controllers\RequestController::class, 'showRelated'])->name('my_books');
