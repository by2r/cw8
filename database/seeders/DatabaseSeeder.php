<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Request;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(ReadersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(BooksTableSeeder::class);
        DB::table('readers')->insert([
            'name' => 'Baitur Bulanbekov',
            'address' => 'Abdrahmanova 132',
            'passport_number' => '123455677',
            'library_card_number' => '100100',
            'password' => 'password'
        ]);
    }
}
