<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Reader>
 */
class ReaderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->firstName,
            'address' => $this->faker->address,
            'passport_number' => $this->faker->numerify('############'),
            'library_card_number' => $this->faker->numerify('############'),
            'password' => 'password'
        ];
    }
}
