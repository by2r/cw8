<?php
namespace App\Http\Controllers;

use App\Http\Requests\SessionRequest;
use App\Models\Reader;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class SessionsController extends AuthController
{
    /**
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function create(Request $request)
    {
        if ($request->session()->exists('library_card_number')) {
            return redirect()->route('home')->with('error', 'You are already login!');
        }
        return view('sessions.create');
    }

    /**
     * @param SessionRequest $request
     * @return RedirectResponse
     */
    public function store(SessionRequest $request)
    {
        $reader = Reader::where('library_card_number', $request->get('library_card_number'))->firstOrFail();
        if ($this->auth($reader, $request->get('password'))) {
            $this->logIn($reader);
            return redirect()->route('home')->with('success', 'You are login success');
        } else {
            return back()->with('error', 'Incorrect email or password');
        }
    }

    /**
     * @return RedirectResponse
     */
    public function destroy(): RedirectResponse
    {
        $this->logOut();
        return redirect()->route('login')->with('success', 'You are success logout');
    }
}
