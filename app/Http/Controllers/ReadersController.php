<?php
namespace App\Http\Controllers;
use App\Http\Requests\RegisterRequest;
use App\Models\Reader;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ReadersController extends AuthController
{
    private int $library = 100000;
    /**
     * @param Request $request
     * @return Application|Factory|View|RedirectResponse
     */
    public function register(Request $request)
    {
        if ($request->session()->exists('library_card_number')) {
            return redirect()->route('home')->with('error', 'You are already registered!');
        }
        return view('readers.register');
    }

    /**
     * @param RegisterRequest $request
     * @return RedirectResponse
     */
    public function store(RegisterRequest $request)
    {
        $payload = collect($request->all());

        $payload['password'] = Hash::make($payload->get('password'));
        $payload['password_confirmation'] = Hash::make($payload->get('password_confirmation'));
        $this->library+=2;
        $payload['library_card_number'] = $this->library;
        $reader = Reader::create($payload->all());
        $this->logIn($reader);
        return redirect()->route('home')->with('success', 'You are register success!');
    }
}
