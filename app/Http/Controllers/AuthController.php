<?php
namespace App\Http\Controllers;
use App\Models\Reader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * @param Reader $reader
     */
    protected function logIn(Reader $reader): void
    {
        session()->put('library_card_number', $reader->library_card_number);
    }

    /**
     * @param Reader $reader
     * @param string $password
     * @return bool
     */
    protected function auth(Reader $reader, string $password): bool
    {
        return Hash::check($password, $reader->password);
    }

    protected function logOut(): void
    {
        session()->remove('library_card_number');
        session()->regenerate();
    }
}
