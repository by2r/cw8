<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestRequest;
use App\Models\Book;
use App\Models\Reader;
use Illuminate\Http\Request;
use Illuminate\View\View;


class RequestController extends Controller
{
    public function makeRequest(Book $book): View {
        return view('request', compact('book'));
    }
    public function store(RequestRequest $request) {
        $re = $request->all();
        $re['book_id'] = 1;
        var_dump($re['library_card_number']);
        $re['reader_id'] = Reader::all()->where('library_card_number', $re['library_card_number'])->firstOrFail()->id;

        \App\Models\Request::create($re);

        return redirect()->route('home')->with('success', 'Your request is approved!');
    }

    public function showRelated() {
        $library_card_number = session()->get('library_card_number');
        $reader = Reader::all()->where('library_card_number', $library_card_number)->firstOrFail();
        return \view('readers.books', compact('reader'));
    }
}
