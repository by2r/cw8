<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
    protected $fillable = ['image', 'author', 'title', 'category_id', 'status'];
    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function request() {
        return $this->hasMany(Request::class);
    }
}
