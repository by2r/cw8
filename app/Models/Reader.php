<?php

namespace App\Models;

use Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reader extends Model
{
    use HasFactory;
    public int $library = 100000;
    protected $fillable = ['name', 'address', 'passport_number', 'library_card_number', 'password'];
    protected $casts = [
        'password' => 'hashed',
    ];

    public function requests() {
        return $this->hasMany(Request::class);
    }
}
